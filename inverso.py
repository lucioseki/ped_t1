#!/usr/bin/env python3
import sys

def prnt(a, fname=sys.stdout):
    for e in a[:-1]:
        print(e, file=fname, end=' ')
    print(a[-1], file=fname)

def gen(n):
    a = [x for x in range(int(n))]

    prnt(a, sys.stderr)
    prnt(a, sys.stderr)

    a.reverse()

    print(len(a))
    prnt(a)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        gen(sys.argv[1])
