/*
 * Implementacao de listas com nó cabeça.
 */

#ifndef LISTAD_H
#define LISTAD_H

#define _DIM_ARR_LIMIT_ 32768

typedef struct elem_dim {
    int indice;
#ifdef _ADICIONA_MEMBRO_
    char arr[_DIM_ARR_LIMIT_];  // representa variáveis adicionais
#endif
} elem_dim;

typedef struct no{
    elem_dim item;
    struct no *prox;
} No_lista;

typedef No_lista* ListaDim;

/* Retorna um elem_dim */
void dimElem(elem_dim* elem, int indice);

/* Inicializa uma lista */
void dimInicLista(ListaDim *p_l);

/* Verifica se a lista est� vazia ou nao */
int dimListaVazia(ListaDim *p_l);

/* Insere um elemento no inicio da lista */
void dimInsereInicio(ListaDim *p_l, elem_dim e);

/* Insere um elemento no final da lista */
void dimInsereFim(ListaDim *p_l, elem_dim e);

/* Insere um elemento na lista de maneira ordenada.
   Caso a lista nao esteja ordenada, ordena antes da insercao */
void dimInsereOrdenado(ListaDim *p_l, elem_dim e);

/* Verifica se a lista esta ordenada */
int dimOrdenada(ListaDim *p_l);

/* Ordena a lista */
void dimOrdena(ListaDim *p_l);

/* Remove o elemento que esta no inicio da lista.
   Retorna 0 caso a lista esteja vazia */
int dimRemoveInicio(ListaDim *p_l, elem_dim *p_e);

/* Remove o elemento que esta no final da lista.
   Retorna 0 caso a lista esteja vazia */
int dimRemoveFim(ListaDim *p_l, elem_dim *p_e);

/* Remove o numero de valor e.
   Retorna 0 caso este numero n�o tenha sido encontrado */
int dimRemoveValor(ListaDim *p_l, elem_dim e);

/* Inverte os elementos de uma lista */
void dimInverte(ListaDim *p_l);

/* Remove todos os numeros da lista */
void dimLibera(ListaDim *p_l);

/* Exibe o conteudo da lista */
void dimExibe(ListaDim *p_l);

/* Retorna o tamanho da lista */
int dimTamanho(ListaDim *p_l);

#endif
