---
title: 'Relatório - Ordenação: Lista Estática VS Lista Dinâmica'
author: Lucio Seki
geometry: margin=1.5cm
---

---
link-citations: true
reference-section-title: Referências
references:
- type: book
  title: "Algoritmos: teoria e prática"
  author:
  - family: Cormen et al.
  id: cormen2002
  issued:
  - date-parts:
  - - 2002


- URL: http://repo.or.cz/w/glibc.git/blob/HEAD:/stdlib/qsort.c
  title: "stdlib/qsort.c"
  author:
  - family: GNU Project
  issued:
  - date-parts:
  - - 2017
  id: glibc

- URL: http://man7.org/linux/man-pages/man1/time.1.html
  title: "GNU Time"
  author:
  - family: GNU Project
  issued:
  - date-parts:
  - - 2017
    - 01
    - 22
  id: gnutime

- URL: https://www.postgresql.org/docs/9.5/static/storage-toast.html
  title: "TOAST (The Oversized-Attribute Storage Techinique)"
  author:
  - family: The PostgreSQL Global Development Group
  issued:
  - date-parts:
  - - 2017
  id: postgres-toast

- URL: http://stackoverflow.com/a/7560859
  title: "algorithm - median of three values strategy"
  author:
  - family: Coffin
    given: Jerry
  issued:
  - date-parts:
  - - 2011
    - 09
    - 26
  id: stackoverflow-mediana



---


# Introdução

Neste trabalho escolhi o algoritmo QuickSort [@cormen2002] para implementar a ordenação das listas estática e dinâmica.

Escolhi ele porque apesar de tê-lo utilizado várias vezes, nunca o tinha implementado usando uma lista dinâmica, e tampouco tinha compreendido a sua relação de recorrência no caso médio.

QuickSort é muito utilizado, inclusive pelo glibc [@glibc], pois apesar de ter complexidade $\mathcal{O}(n^2)$ no pior caso, em média executa em $\mathcal{O}(n \log n)$, conforme explicado adiante.


# Descrição do Algoritmo


## Definição

O algoritmo QuickSort utiliza a abordagem divisão e conquista:

- Hipótese de indução: Sabemos ordenar um conjunto $S = \{s_1, s_2, \ldots, s_k\}$, $1 \leq k < n$.
- Caso base: $n = 1$. Um conjunto com um único elemento já está ordenado.
- Passo da indução: Seja $S$ um conjunto a ser ordenado com $n > 1$ elementos, e $x$ um elemento de $S$.
Seja $S_1$ o conjunto $S - x$ tal que seus elemnetos são menores que $x$, e $S_2$ o conjunto $S - x$ tal que seus elementos são maiores que $x$.
Tanto $S_1$ quanto $S_2$ tem menos de $n$ elementos.
Pela hipótese de indução, sabemos ordenar $S_1$ e $S_2$.
Seja $S_1'$ o conjunto $S_1$ ordenado e $S_2'$ o conjunto $S_2$ ordenado, podemos obter o conjunto $S$ ordenado concatenando $S_1'$, $x$ e $S_2'$.


## Implementação

Implementei o algoritmo numa lista estática da seguinte maneira:

`lista_dinamica.c`
``` {.c .numberLines startFrom="102"}
void estQuickSort(ListaEst *p_l, int esq, int dir) {
    int pivot;
    int i, j;
    elem_est temp_elem;

    if (esq < dir) {  // verifica se a lista tem n > 1 elementos
        // aleatoriza a seleção do pivot
#ifdef _RANDOMIZA_PIVOT_
        pivot = esq + rand() % (dir+1 - esq);
        SWAP(temp_elem, p_l->item[esq], p_l->item[pivot]);
#endif

        i = esq - 1;
        // procura por um elemento menor que pivot
        for (j = esq; j < dir; j++) {
            // utiliza o últimoi elemento como pivot
            if (MENOR(p_l->item[j], p_l->item[dir])) {
                i++;
                // permuta i-ésimo e o j-ésimo elementos
                SWAP(temp_elem, p_l->item[i], p_l->item[j]);
            }
        }
        i++;
        // permuta o pivot com
        // o último elemento classificado como maior que pivot
        SWAP(temp_elem, p_l->item[dir], p_l->item[i]);

        // recursão para esquerda e direita do pivot
        // conjunto S1 com elementos menores que o pivot
        estQuickSort(p_l, esq, i - 1);
        // conjunto S2 com elementos maiores que o pivot
        estQuickSort(p_l, i + 1, dir);
    }
}

/* Ordena a lista */
void estOrdena(ListaEst *p_l)
{
    estQuickSort(p_l, 0, p_l->tam-1);
    return;
}
```

Para implementar o algoritmo na lista dinâmica, precisei adaptar a obtenção do n-ésimo elemento na lista, percorrendo-a sequencialmente.
Também precisei adaptar a permutação, para atualizar os ponteiros dos nós.


## Análise de Complexidade

No caso base $n = 1$, o algoritmo realiza 0 operações.
Para $n > 1$, no pior caso, será necessário percorrer por $n - 1$ elementos, quando $S_1$ tiver $n - 1$ elementos e $S_2$ for vazio, ou vice-versa, para toda chamada recursiva.
Nesta implementação baseada em [@cormen2002], o pior caso ocorre quando a lista já está completamente ordenada.

Obtive a relação de recorrência do QuickSort para o pior caso da seguinte forma:

$$
T(n) = 
\begin{cases}
    0,              & n = 1 \\
    T(n-1) + n - 1, & n > 1
\end{cases}
$$ 

que, por substituição iterativa:

$$
\begin{split}
T(n) & = T(n-1) + n - 1 \\
     & = T(n-2) + n - 1 + n - 1 \\
     & = T(n-3) + n - 1 + n - 1 + n - 1 \\
     & \ldots \\
     & = T(n-(n-1)) + n - 1 + n - 1 + \ldots + n - 1 \\
     & = 1 + n - 1 + n - 1 + \ldots + n - 1 \\
     & = 1 + \sum_{i=1}^{n}{n - 1} \\
     & = 1 + n^{2} - n
\end{split}
$$

portanto, $T(n) \in \mathcal{O}(n^{2})$.

No entanto, na média, o algoritmo QuickSort executa em $\mathcal{O}(n \log n)$.

As chamadas recursivas para QuickSort nunca incluem o pivot. Portanto, as instruções que são executadas uma quantidade constante de vezes (que estão fora do laço _for_ da linha $116-123$), resultam na complexidade $\mathcal{O}(n)$.

Seja $X$ a quantidade total de vezes que as instruções dentro do laço _for_ são executadas, a complexidade do algoritmo é $\mathcal{O}(n + X)$.

Dentro deste laço há basicamente uma comparação entre dois elementos do mesmo subconjunto, e uma permutação dependendo do resultado da comparação. Para obter o valor de $X$, é necessário descobrir quando dois elementos são comparados. Ou seja, obter a esperança $E(A)$ a partir de $A$ = { probabilidade de $z_i$ e $z_j$ serem comparados } dentro do subconjunto $Z_{ij} = \{z_i, z_{i+1}, z_{i+2}, \ldots, z_j\}$.

Dois elementos são comparados somente quando estão no mesmo subconjunto, e somente se um dos elementos for o pivot, pois as chamadas recursivas utilizam subconjuntos excluindo o pivot.

Na implementação com a seleção aleatória do pivot, dado um subconjunto $S$, a probabilidade de $z_i$ ser escolhido como pivot é de $\frac{1}{i - j + 1}$, e a probabilidade de $z_j$ ser escolhido como pivot também é de $\frac{1}{i - j + 1}$. Logo, $P(A) = \frac{2}{i - j + 1}$.

Portanto, considerando o limite da série harmônica $\frac{2}{k} = \mathcal{O}(\log n)$, e $k = j - i$, a esperança $E(A)$ é:

$$
\begin{split}
E(A) & = \sum_{i=1}^{n-1}{\sum_{j=i+1}^{n}{\dfrac{2}{i-j+1}}} \\
     & = \sum_{i=1}^{n-1}{\sum_{k=1}^{n-1}{\dfrac{2}{k}}} \\
     & < \sum_{i=1}^{n-1}{\mathcal{O}(\log n)} \\
     & = \mathcal{O}(n \log n)
\end{split}
$$

Logo, no caso médio, o algoritmo QuickSort possui complexidade $\mathcal{O}(n \log n)$.

A implementação utilizando lista dinâmica acrescenta uma quantidade constante de operações para atualizar os ponteiros, e uma quantidade linear de operações para percorrer a lista para obter o n-ésimo elemento. Como esta busca é feita em todo nível de recursão, aumenta o grau de complexidade para $\mathcal{O}(n^2)$.


# Detalhes de Implementação


## Lista dinâmica

Implementei a lista dinâmica sem nó cabeça. Ou seja, o primeiro nó também armazena um elemento.

Redefini o tipo `ListaDim` para ser um ponteiro para `No_lista`:

`lista_dinamica.h`
``` {.c .numberLines startFrom="19"}

typedef No_lista* ListaDim;


```

Também adaptei as funções de inserção e remoção que já existiam, para operar com o novo tipo.


## Seleção aleatória do pivot

Para evitar o desempenho de pior caso, a seleção do pivot foi aleatorizado.
Desta forma, mesmo que a entrada seja uma lista já ordenada, o desempenho será em média de $\mathcal{O}(n \log n)$, conforme já explicado.

Uma macro foi definida para habilitar ou desabilitar esta funcionalidade, para permitir a comparação de desempenho em ambas situações.


## Execução dos testes


### Macros e Targets

Para facilitar a execução dos testes, criei condições que verificam a existência de algumas macros.
Passando-as como parâmetro ao compilador durante o preprocessamento, elas afetam a execução da seguinte maneira:

- `_IMPRIME_TEMPO_`: o teste imprime o tempo (_walltime_) gasto para cada caso de teste;

- `_IMPRIME_CPU_`: o teste imprime o tempo de cpu gasto para cada caso de teste;

- `_IMPRIME_LISTA_`: o teste imprime o resultado da ordenação da lista;

- `_IMPRIME_TAMANHO_`: o teste imprime o tamanho da entrada para cada caso de teste;

- `_EXEC_EST_`: executa o teste referente à lista estática;

- `_EXEC_DIM_`: executa o teste referente à lista dinâmica;

- `_ADICIONA_MEMBRO_`: adiciona um membro adicional na `struct elem_est` e na `struct elem_dim`, conforme descrito na próxima seção;

- `_RANDOMIZA_PIVOT_`: habilita a seleção aleatória do pivot no quicksort.

As quatro últimas macros estão definidas no início do `Makefile`, e podem ser comentados para desabilitar a funcionalidade.

Criei _targets_ no `Makefile` que realizam determinados testes:

- default: Atende às especificações do trabalho. Imprime o tempo de execução _walltime_ e o resultado da ordenação de ambas as listas;
- test: Valida se a ordenação foi implementada corretamente. Gera e roda um executável que exibe somente a lista após a ordenação, e compara a saída com a saída esperada;
- time: Exibe o tempo de execução em função do tamanho da entrada. Gera e roda um executável que exibe o tamanho da entrada, o tempo de execução (walltime e CPU) e o uso de memória [@gnutime] de cada caso de teste.


### Casos de teste

E, por fim, criei vários casos de teste no diretório `testes/`.

Os arquivos seguem a nomenclatura `<tipo><tamanho>.<ext>`.

`<tipo>` é o tipo da entrada que pode ser:

- 'best': inteiros em $[0, <tamanho>)$, na ordem que resulta em melhor caso;

- 'rand': inteiros em $[0, <tamanho>)$ ordenados aleatoriamente;

- 'inv':  inteiros em $[0, <tamanho>)$, em ordem decrescente;

- 'seq':  inteiros em $[0, <tamanho>)$, em ordem crescente. É o pior caso.


`<tamanho>` é o tamanho da entrada. Utilizei tamanhos 10, 5000, 10000 e 15000.

`<ext>` indica se é a entrada (`in`) ou a saída esperada (`out`).

Por exemplo, `inv15000.in` é um caso de teste contendo inteiros de 0 a 14999 em ordem decrescente,
e `rand5000.out` é a saída esperada para o caso de teste com inteiros aleatórios entre 0 e 4999.


### Melhor caso

O melhor caso nesta implementação do QuickSort ocorre quando, em cada nível de recursão, os subconjuntos possuem tamanhos
$\lfloor n/2 \rfloor$ e
$\lfloor n/2 \rfloor + 1$.

O algoritmo abaixo é uma forma de preencher um conjunto para atender esta condição:

$$
\begin{split}
& Preenche(A, esq, dir): \\
& A[dir] = \left\lfloor (esq + dir)/2 \right\rfloor \\
& A\left[\left\lfloor (esq + dir)/2 \right\rfloor \right] = dir \\
& Preenche\left(A, esq, \left\lfloor (esq + dir)/2 \right\rfloor - 1 \right) \\
& Preenche\left(A, \left\lfloor (esq + dir)/2 \right\rfloor + 1, dir - 1 \right)
\end{split}
$$

Por exemplo, a lista abaixo foi gerado por este algoritmo:

+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 0   | 2   | 1   | 6   | 5   | 4   | 3   | 14  | 9   | 8   | 13  | 12  | 11  | 10  | 7   |
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+


# Discussão e Análise


## Alteração dos elementos

Acrescentei um membro na `struct elem_est` para representar as variáveis adicionais que podem existir num cenário mais real:

`lista_estatica.h`
``` {.c .numberLines startFrom="10"}
#define _EST_ARR_LIMIT_ 32768

typedef struct elem_est {
    int indice;
#ifdef _ADICIONA_MEMBRO_
    char arr[_EST_ARR_LIMIT_];  // representa variáveis adicionais
#endif
} elem_est;

typedef struct {
    elem_est item[MAX];
    int tam;
} vet;
```

Para fins desta análise, tanto faz adicionar como membro um único vetor de 32768 _bytes_ ou 4096 ponteiros de 8 _bytes_,
pois a forma de alocação de memória para a _struct_ como um todo será equivalente.
Por isso, acrescentei apenas um membro `char arr[_EST_ARR_LIMIT_]`.

Como a ordenação opera somente sobre o `indice`, o valor do membro adicional não interfere no desempenho.
Por isso, o vetor `arr` não foi inicializado.

A função principal declara uma `ListaEst`, que por sua vez contém um vetor de `MAX (15000)` elementos `struct elem_est`.
Para evitar _stack overflow_, removi o limite do tamanho da _stack_ com o comando `ulimit -s unlimited`.

Da mesma forma, acrescentei um membro `char arr[_DIM_ARR_LIMIT_]` na `struct elem_dim`:

`lista_dinamica.h`
``` {.c .numberLines startFrom="8"}
#define _DIM_ARR_LIMIT_ 32768

typedef struct elem_dim {
    int indice;
#ifdef _ADICIONA_MEMBRO_
    char arr[_DIM_ARR_LIMIT_];  // representa variáveis adicionais
#endif
} elem_dim;
```


## Resultado dos testes

Os testes descritos acima foram executados num computador com as especificações abaixo:

```
Fabricante: Lenovo
Modelo: ThinkPad X240
CPU: Intel(R) Core(TM) i7-4600U CPU @ 2.10GHz
Memória: 8G 1600MHz DDR3L
```

Relacionei nos gráficos abaixo o resultado de cada caso de teste.

Executei cada caso de teste 3 vezes, e utilizei o resultado contendo o valor mediano considerando primeiro o tempo _wallclock_, depois o tempo de CPU e por fim o consumo de memória.


### Tempo de CPU

Utilizei escala logarítmica para melhor visualização.


#### Lista estática

![](images/cpu_est_norand_nomemb.png) ![](images/cpu_est_rand_nomemb.png)  

![](images/cpu_est_norand_memb.png) ![](images/cpu_est_rand_memb.png)

É possível observar que a aleatorização do pivot traz um desempenho muito próximo do melhor caso.

O acréscimo do membro adicional prejudica consideravelmente o desempenho da ordenação,
pois a operação de permutação fica muito mais custosa.

\newpage

#### Lista dinâmica

![](images/cpu_dim_norand_nomemb.png) ![](images/cpu_dim_rand_nomemb.png)

![](images/cpu_dim_norand_memb.png) ![](images/cpu_dim_rand_memb.png)

O desempenho da lista dinâmica é muito pior do que a da lista estática.

O acréscimo do membro adicional não influencia muito no desempenho da ordenação,
pois a permutação opera somente sobre os ponteiros,
ao contrário da lista estática que copia a _struct_ inteira.

Também é possível observar que, com a aleatorização do pivot, várias horas são poupadas,
se aproximando do melhor caso.

Note que o melhor caso deixa de ter o melhor desempenho em alguns casos,
pois o algoritmo deixa de subdividir o problema em tamanhos exatamente iguais.


#### Tempo _wallclock_

Relacionei nos gráficos abaixo o tempo _wallclock_ consumido em cada caso de teste.
Para facilitar a visualização, utilizei escala logarítmica assumindo $0s = 0.1s$.

\newpage

#### Lista estática

![](images/wall_est_norand_nomemb.png) ![](images/wall_est_rand_nomemb.png)  

![](images/wall_est_norand_memb.png) ![](images/wall_est_rand_memb.png)


#### Lista dinâmica

![](images/wall_dim_norand_nomemb.png) ![](images/wall_dim_rand_nomemb.png)

![](images/wall_dim_norand_memb.png) ![](images/wall_dim_rand_memb.png)

A relação é muito similar ao que foi ilustrado nos gráficos de tempo de CPU.


#### Consumo de memória

Relacionei nos gráficos abaixo o tamanho da entrada com o consumo de memória pelos casos de teste.

Para facilitar a visualização, utilizei escala logarítmica para casos com membro adicional.


#### Lista estática

![](images/mem_est_norand_nomemb.png) ![](images/mem_est_rand_nomemb.png)  

![](images/mem_est_norand_memb.png) ![](images/mem_est_rand_memb.png)

Os piores casos utilizam mais memória, quando não há seleção aleatória do pivot.

Isto ocorre porque há mais chamadas recursivas do QuickSort,
e portanto mais uso da pilha de execução.


#### Lista dinâmica

![](images/mem_dim_norand_nomemb.png) ![](images/mem_dim_rand_nomemb.png)

![](images/mem_dim_norand_memb.png) ![](images/mem_dim_rand_memb.png)

O consumo de memória é proporcional ao tamanho da entrada, e também à quantidade de chamadas recursivas.

Na lista estática isso é mais evidente, porque a cada chamada recursiva,
há uma nova variável local `struct elem_est` sendo empilhada na pilha de execução.


# Conclusão

Implementei o algoritmo QuickSort utilizando uma lista estática e uma lista dinâmica. Comparando os resultados, verifiquei que a lista estática possui um desempenho melhor em todos os casos.

A lista estática permite acesso ao x-ésimo elemento em tempo constante, enquanto que a lista estática precisa ser percorrida seguindo x ponteiros. O QuickSort necessita desta operação a cada nível de recursão, e isso impacta no desempenho da lista dinâmica.

O acréscimo de um membro grande na _struct_ prejudicou bastante o desempenho na lista estática, pois a carga da operação de leitura e escrita em memória foi aumentada milhares de vezes.

Já na lista dinâmica, o impacto foi pouco, pois as operações são feitas somente sobre o índice e os ponteiros.

A aleatorização do pivot permitiu um desempenho muito próximo ao do melhor caso, para todas as entradas. Já a versão sem aleatorização obteve desempenho de pior caso numa lista já ordenada, e um desempenho muito próximo ao do pior caso numa lista inversamente ordenada.

O consumo de memória da lista estática em si é de aproximadamente 2MB, sem o membro adicional. No entanto, a cada chamada recursiva, a pilha de execução aumenta alocando memória para, entre outras variáveis, uma `struct elem_est` temporária para realizar a permutação.

A lista dinâmica também utiliza a pilha de execução, mas apresenta um consumo mais otimizado de memória, alocando somente o necessário e operando somente com ponteiros, sem ficar empilhando uma `struct` adicional a cada chamada recursiva.


## Oportunidades de melhoria

Várias melhorias podem ser feitas:

- Para diminuir o consumo de memória do QuickSort na lista estática, em vez de empilhar na pilha de execução uma `struct elem_est` local em toda chamada recursiva, poderia declará-la globalmente, pois ela é utilizada apenas como uma variável temporária para permutação.

- Para diminuir a complexidade do QuickSort na lista dinâmica, poderia utilizar um vetor de ponteiros que armazenam o endereço de memória de cada nó.
Assim, o acesso ao x-ésimo elemento pode ser feito em tempo constante, assim como na lista estática.

- Para diminuir o impacto do membro adicional, poderia utilizar índice externa, como é feito num cenário real [@postgres-toast]. Assim, a ordenação é feita somente sobre o índice e alguns poucos metadados.

- Uma implementação não recursiva dispensaria o _overhead_ pelo uso da pilha de execução, diminuindo consideravelmente o tempo de execução e o consumo de memória.

- Poderia utilizar o particionamento mediana-de-três [@stackoverflow-mediana], escolhendo o pivot que seja a mediana dentre o primeiro, o último e o central dentro do subconjunto. Isto garante que uma lista já ordenada resulte no desempenho de melhor caso, e dispensa a complexidade da geração de um número aleatório.


# Referências

