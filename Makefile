# fonte: https://stackoverflow.com/questions/1484817

MAIN = teste_lista.c
TARGET = teste_lista
TARGET_TESTE = teste_lista_teste
TARGET_TIME = teste_lista_time
LIBS = -lm
CC = gcc

# comente as linhas abaixo para desabilitar funcionalidades
# adiciona membro adicional ao elemento da lista
ADD_MEMBRO=-D_ADICIONA_MEMBRO_
# aleatoriza a seleção do pivot
RANDOMIZA=-D_RANDOMIZA_PIVOT_
# executa teste em ambas as listas
EXECUTA=-D_EXEC_EST_ -D_EXEC_DIM_

CFLAGS = -g -Wall -O0 $(RANDOMIZA) $(EXECUTA) $(ADD_MEMBRO)
PDF = lucio_trab1.pdf
ZIP = lucio_trab1.zip

.PHONY: default all clean

default: $(TARGET)
all: default

# `make test` gera executável sem exibir o tempo de execução
# executa os testes e compara as saídas
test: $(TARGET_TESTE)
	for f in $$(ls testes/*10.in); do \
	echo $${f}; \
	./$(TARGET_TESTE) < $${f} > minhasaida.out; \
	diff -q testes/$$(basename $${f} | cut -f 1 -d '.').out minhasaida.out; \
	if [ $$? -eq 0 ]; then \
	echo SUCESSO; \
	fi \
	done

# `make time` gera e executa executável que exibe o tamanho da entrada e não exibe a lista ordenada
time: $(TARGET_TIME)
	for i in {1..3}; do \
	for f in $$(ls testes/*.in); do \
	echo $${f}; \
	\time -v ./$(TARGET_TIME) < $${f} 2>&1 | tee testes/time_$$(basename $${f} | cut -f 1 -d '.')_$${i}.out; \
	done \
	done

# o arquivo com a função principal é compilada à parte, para que possa preprocessar a macro _PRINT_TIME_
OBJECTS = $(subst gerador.o,,$(patsubst %.c, %.o, $(filter-out teste_lista.c, $(wildcard *.c))))
HEADERS = $(wildcard *.h)
SOURCES = $(filter-out gerador.c, $(wildcard *.c))
GPFILES = $(wildcard resultados/*.gp)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

# o target de time imprime o tamanho da entrada e o tempo de execução (walltime e cpu)
$(TARGET_TIME): $(OBJECTS) $(MAIN)
	$(CC) teste_lista.c -D_IMPRIME_TAMANHO_ -D_IMPRIME_TEMPO_ -D_IMPRIME_CPU_ $(CFLAGS) $(OBJECTS) -Wall $(LIBS) -o $@

# o target de teste imprime sem tempo de execução (walltime)
$(TARGET_TESTE): $(OBJECTS) $(MAIN)
	$(CC) teste_lista.c -D_IMPRIME_LISTA_ $(CFLAGS) $(OBJECTS) -Wall $(LIBS) -o $@

# o target default imprime tempo de execução (walltime) e a lista ordenada
$(TARGET): $(OBJECTS) $(MAIN)
	$(CC) teste_lista.c -D_IMPRIME_TEMPO_ -D_IMPRIME_LISTA_ $(CFLAGS) $(OBJECTS) -Wall $(LIBS) -o $@

zip: $(ZIP)

$(ZIP): $(SOURCES) $(filter-out timer.h, $(HEADERS)) $(PDF) Makefile teste.in teste.out
	zip $(ZIP) $^

pdf: $(PDF) relatorio.md $(GPFILES)

$(PDF): relatorio.md $(GPFILES)
	gnuplot $(GPFILES)
	pandoc --filter pandoc-citeproc -o $@ relatorio.md

clean:
	-rm -f *.o
	-rm -f $(TARGET) $(TARGET_TESTE) $(TARGET_TIME)
	-rm -f minhasaida.out
	-rm -f $(ZIP)
	-rm -f $(PDF)
