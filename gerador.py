#!/usr/bin/env python3

"""
Gera entrada e saída para teste_lista

Recebe como argumento a quantidade n de elementos na lista.

Imprime a entrada de duas linhas:
    na primeira imprime a quantidade n de elementos
    na segunda imprime n números inteiros de 0 a n-1,
       embaralhados, separados por espaços.
E imprime a saída de duas linhas, idênticas,
    de n números inteiros ordenados de 0 a n-1

A entrada é impressa no stdout
e a saída é impressa no stderr
"""

from random import shuffle
import sys

def prnt(a, fname=sys.stdout):
    for e in a[:-1]:
        print(e, file=fname, end=' ')
    print(a[-1], file=fname)

def gen(n):
    a = [x for x in range(int(n))]

    prnt(a, sys.stderr)
    prnt(a, sys.stderr)

    shuffle(a)

    print(len(a))
    prnt(a)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        gen(sys.argv[1])
