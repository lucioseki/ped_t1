/*
 * Implementacao de lista estatica.
 */

#ifndef LISTAE_H
#define LISTAE_H

#define MAX 15000 // Sim, isto está certo e sua lista deve comportar no máximo 15 mil elementos

#define _EST_ARR_LIMIT_ 32768

typedef struct elem_est {
    int indice;
#ifdef _ADICIONA_MEMBRO_
    char arr[_EST_ARR_LIMIT_];  // representa variáveis adicionais
#endif
} elem_est;

typedef struct {
    elem_est item[MAX];
    int tam;
} vet;

typedef vet ListaEst;

/* Retorna um elem_est */
void estElem(elem_est* elem, int indice);

/* Inicializa uma lista */
void estInicLista(ListaEst *p_l);

/* Verifica se a lista est� vazia ou nao */
int estListaVazia(ListaEst *p_l);

/* Insere um elemento no inicio da lista */
void estInsereInicio(ListaEst *p_l, elem_est e);

/* Insere um elemento no final da lista */
void estInsereFim(ListaEst *p_l, elem_est e);

/* Insere um elemento na lista de maneira ordenada.
   Caso a lista nao esteja ordenada, ordena antes da insercao */
void estInsereOrdenado(ListaEst *p_l, elem_est e);

/* Verifica se a lista esta ordenada */
int estOrdenada(ListaEst *p_l);

/* Ordena a lista */
void estOrdena(ListaEst *p_l);

/* Remove o elemento que esta no inicio da lista.
   Retorna 0 caso a lista esteja vazia */
int estRemoveInicio(ListaEst *p_l, elem_est *p_e);

/* Remove o elemento que esta no final da lista.
   Retorna 0 caso a lista esteja vazia */
int estRemoveFim(ListaEst *p_l, elem_est *p_e);

/* Remove o numero de valor e.
   Retorna 0 caso este numero n�o tenha sido encontrado */
int estRemoveValor(ListaEst *p_l, elem_est e);

/* Inverte os elementos de uma lista */
void estInverte(ListaEst *p_l);

/* Remove todos os numeros da lista */
void estLibera(ListaEst *p_l);

/* Exibe o conteudo da lista */
void estExibe(ListaEst *p_l);

elem_est estObtemElem(ListaEst *p_l, int indice);

#endif
