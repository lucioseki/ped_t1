/*
 * Implementação baseada na feita por: Maurício Spinardi
 */

/*
 * Implementacao de lista dinamica com nó cabeça.
 */

#include "lista_dinamica.h"
#include <stdio.h>
#include <stdlib.h>

// macros para comparação entre elementos
#define MENOR(a, b) ((a).indice < (b).indice)
#define IGUAL(a, b) ((a).indice == (b).indice)
#define MAIOR(a, b) ((a).indice > (b).indice)

/* Inicializa um elem_dim */
void dimElem(elem_dim* elem, int indice) {
    elem->indice = indice;
}

/* Inicializa uma lista */
void dimInicLista(ListaDim *p_l)
{
    *p_l = NULL;
}

/* Verifica se a lista está vazia ou nao */
int dimListaVazia(ListaDim *p_l)
{
    return *p_l == NULL;
}

/* Insere um elemento no inicio da lista */
void dimInsereInicio(ListaDim *p_l, elem_dim e)
{
    No_lista *novo;

    novo = (No_lista *) malloc (sizeof (No_lista));
    novo->item = e;
    if (dimListaVazia(p_l)) {
        novo->prox = NULL;
        *p_l = novo;
    } else {
        novo->prox = (*p_l)->prox;
        (*p_l)->prox = novo;
    }
}

/* Insere um elemento no final da lista */
void dimInsereFim(ListaDim *p_l, elem_dim e)
{
    No_lista *novo, *aux;

    if (dimListaVazia(p_l)) {
        dimInsereInicio(p_l, e);
    } else {
        aux = *p_l;
        while (aux->prox != NULL) {
            aux = aux->prox;
        }

        novo = (No_lista *) malloc (sizeof (No_lista));
        novo->item = e;
        novo->prox = NULL;
        aux->prox = novo;
    }
}

/* Insere um elemento na lista de maneira ordenada.
   Caso a lista nao esteja ordenada, ordena antes da insercao */
void dimInsereOrdenado(ListaDim *p_l, elem_dim e)
{
    No_lista *novo, *aux;

    if (!dimOrdenada (p_l))
        dimOrdena (p_l);

    if (dimListaVazia (p_l))
        return dimInsereInicio (p_l, e);

    aux = *p_l;
    while (MENOR(aux->item, e)) {
        aux = aux->prox;
        if (aux->prox == NULL) break;
    }

    novo = (No_lista *) malloc (sizeof (No_lista));
    novo->item = e;
    novo->prox = aux->prox;
    aux->prox = novo;
}

/* Verifica se a lista esta ordenada */
int dimOrdenada(ListaDim *p_l)
{
    No_lista *aux;

    if (dimListaVazia (p_l))
        return 1;

    aux = *p_l;
    while (aux->prox != NULL) {
        if (MAIOR(aux->item, aux->prox->item)) break;
        aux = aux->prox;
    }

    if (aux->prox == NULL)
        return 1;

    return 0;
}

/* Retorna ponteiro para o elemento na posição pos
   Retorna NULL caso não exista */
No_lista* dimObtemElem(ListaDim* p_l, int pos) {
    No_lista *aux = NULL;

    if (dimListaVazia(p_l) || pos < 0) {
        return NULL;
    }

    aux = *p_l;
    while (pos > 0) {
        aux = aux->prox;
        pos--;
    }

    return aux;
}

/* Permuta a com b, atualizando os ponteiros */
void dimSwap(ListaDim *p_l, int a, No_lista *no_a, int b, No_lista *no_b) {
    No_lista *temp;
    No_lista *ant_a, *ant_b;

    ant_a = dimObtemElem(p_l, a-1);  // nó que precede no_a
    ant_b = dimObtemElem(p_l, b-1);  // nó que precede no_b

    if (ant_a) {
        ant_a->prox = no_b;
    } else {
        // se no_a for o primeiro nó da lista, no_b passa a ser o primeiro
        *p_l = no_b;
    }
    if (ant_b) {
        ant_b->prox = no_a;
    } else {
        // se no_b for o primeiro nó da lista, no_a passa a ser o primeiro
        *p_l = no_a;
    }

    temp = no_a->prox;
    no_a->prox = no_b->prox;
    no_b->prox = temp;
}

/* Implementação do QuickSort */
void dimQuickSort(ListaDim *p_l, int esq, int dir) {
    int i, j;
    int pivot;
    No_lista *no_dir, *no_i, *no_j;

    if (esq < dir) {
#ifdef _RANDOMIZA_PIVOT_
        // aleatoriza a seleção do pivot
        pivot = esq + rand() % (dir+1 - esq);
        no_i = dimObtemElem(p_l, pivot);
        no_j = dimObtemElem(p_l, esq);
        dimSwap(p_l, pivot, no_i, esq, no_j);
#endif

        no_dir = dimObtemElem(p_l, dir);

        i = esq - 1;
        for (j = esq; j < dir; j++) {
            no_j = dimObtemElem(p_l, j);
            if (MENOR(no_j->item, no_dir->item)) {
                i++;
                no_i = dimObtemElem(p_l, i);
                dimSwap(p_l, i, no_i, j, no_j);
            }
        }
        i++;
        no_i = dimObtemElem(p_l, i);
        no_dir = dimObtemElem(p_l, dir);
        dimSwap(p_l, dir, no_dir, i, no_i);

        // recursão para esquerda e direita do pivot
        dimQuickSort(p_l, esq, i - 1);
        dimQuickSort(p_l, i + 1, dir);
    }
}

/* Retorna o tamanho da lista */
int dimTamanho(ListaDim *p_l) {
    int i = 0;
    No_lista *aux;

    if (!dimListaVazia(p_l)) {
        i++;
        aux = *p_l;
        while ((aux = aux->prox)) {
            i++;
        }
    }

    return i;
}

/* Ordena a lista */
void dimOrdena(ListaDim *p_l)
{
    dimQuickSort(p_l, 0, dimTamanho(p_l)-1);
    return;
}

/* Remove o elemento que esta no inicio da lista.
   Retorna 0 caso a lista esteja vazia */
int dimRemoveInicio(ListaDim *p_l, elem_dim *p_e)
{
    No_lista *aux;

    if (dimListaVazia (p_l))
        return 0;

    *p_e = (*p_l)->item;
    aux = (*p_l);
    *p_l = (*p_l)->prox;
    free (aux);

    return 1;
}

/* Remove o elemento que esta no final da lista.
   Retorna 0 caso a lista esteja vazia */
int dimRemoveFim(ListaDim *p_l, elem_dim *p_e)
{
    No_lista *aux;

    if (dimListaVazia (p_l))
        return 0;

    aux = *p_l;
    while (aux->prox != NULL)
        aux = aux->prox;

    *p_e = aux->prox->item;
    free (aux->prox);
    aux->prox = NULL;

    return 1;
}

/* Remove o numero de valor e.
   Retorna 0 caso este numero não tenha sido encontrado */
int dimRemoveValor(ListaDim *p_l, elem_dim e)
{
    No_lista *aux, *aux_lista;

    if (dimListaVazia (p_l))
        return 0;

    aux_lista = *p_l;
    while (!IGUAL(aux_lista->item, e)) {
        aux_lista = aux_lista->prox;
        if (aux_lista->prox == NULL) break;
    }

    if (aux_lista->prox == NULL)
        return 0;

    aux = aux_lista->prox;
    aux_lista->prox = aux_lista->prox->prox;
    free (aux);

    return 1;
}

/* Inverte os elementos de uma lista */
void dimInverte(ListaDim *p_l)
{
    ListaDim l;
    No_lista *aux;

    if (!dimListaVazia(p_l)) {
        aux = *p_l;

        dimInicLista (&l);

        while (aux != NULL) {
            dimInsereInicio (&l, aux->item);
            aux = aux->prox;
        }

        dimLibera (p_l);

        *p_l = l;
    }
}

/* Remove todos os numeros da lista */
void dimLibera(ListaDim *p_l)
{
    No_lista *aux, *aux_lista;

    aux = (No_lista *) malloc (sizeof (No_lista));

    if (!dimListaVazia(p_l)) {
        aux_lista = *p_l;
        while (aux_lista != NULL)
            dimRemoveInicio (&aux_lista, &aux->item); // inclui 'free'
    }

    free (aux);
}

/* Exibe o conteudo da lista */
void dimExibe(ListaDim *p_l)
{
    No_lista *aux;

    if (dimListaVazia (p_l)) {
        printf ("Lista vazia!\n");
        return;
    }

    aux = *p_l;
    while (aux != NULL && aux->prox != NULL) {
        printf ("%d ", aux->item.indice);
        aux = aux->prox;
    }
    // Exibe último item sem espaço no final
    printf ("%d", aux->item.indice);

    printf ("\n");
}
