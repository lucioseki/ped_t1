#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* Retorna um inteiro no intervalo [0, n).
 *
 * fonte: https://stackoverflow.com/questions/822323
 *
 * ATENÇÃO: Isso não é um gerador confíavel, mas é suficiente para teste.
 */
int randint(int n) {
    if ((n - 1) == RAND_MAX) {
        return rand();
    } else {
        // Chop off all of the values that would cause skew...
        long end = RAND_MAX / n; // truncate skew
        assert (end > 0L);
        end *= n;

        // ... and ignore results from rand() that fall above that limit.
        // (Worst case the loop condition should succeed 50% of the time,
        // so we can expect to bail out of this loop pretty quickly.)
        int r;
        while ((r = rand()) >= end);

        return r % n;
    }
}

int main()
{
    printf("%d\n", randint(100));
    printf("%d\n", randint(10000));

    return 0;
}
