/*
 * Implementação baseada na feita por: Maurício Spinardi
 */

/*
 * Implementacao de lista estatica.
 */

#include "lista_estatica.h"
#include <stdio.h>
#include <stdlib.h>

// macros para comparação entre elementos
#define MENOR(a, b) ((a).indice < (b).indice)
#define IGUAL(a, b) ((a).indice == (b).indice)
#define MAIOR(a, b) ((a).indice > (b).indice)

// macro para permutar elementos
#define SWAP(temp, a, b) { (temp) = (a); (a) = (b); (b = temp); }

/* Inicializa um elem_est */
void estElem(elem_est* elem, int indice) {
    elem->indice = indice;
}

/* Inicializa uma lista */
void estInicLista(ListaEst *p_l)
{
    p_l->tam = 0;
}

/* Verifica se a lista está vazia ou nao */
int estListaVazia(ListaEst *p_l)
{
    if (!p_l->tam)
        return 1;

    return 0;
}

/* Insere um elemento no inicio da lista */
void estInsereInicio(ListaEst *p_l, elem_est e)
{
    int i;

    if (p_l->tam == MAX)
        return;

    for (i = p_l->tam + 1; i > 0; i--)
        p_l->item[i] = p_l->item[i - 1];

    p_l->item[0] = e;
    p_l->tam++;
}

/* Insere um elemento no final da lista */
void estInsereFim(ListaEst *p_l, elem_est e)
{
    if (p_l->tam == MAX)
        return;

    p_l->item[p_l->tam++] = e;
}

/* Insere um elemento na lista de maneira ordenada.
   Caso a lista nao esteja ordenada, ordena antes da insercao */
void estInsereOrdenado(ListaEst *p_l, elem_est e)
{
    int i, j;

    if (p_l->tam == MAX)
        return;

    if (!estOrdenada (p_l))
        estOrdena (p_l);

    for (i = 0; i < p_l->tam; i++)
        if (MAIOR(p_l->item[i], e)) break;

    for (j = p_l->tam + 1; j > i; j--)
        p_l->item[j] = p_l->item[j - 1];

    p_l->item[i] = e;
    p_l->tam++;
}

/* Verifica se a lista esta ordenada */
int estOrdenada(ListaEst *p_l)
{
    int i;

    for (i = 0; i < p_l->tam - 1; i++)
        if (MAIOR(p_l->item[i], p_l->item[i + 1])) break;

    if (i == p_l->tam - 1)
        return 1;

    return 0;
}

/* Implementação do QuickSort */
void estQuickSort(ListaEst *p_l, int esq, int dir) {
    int pivot;
    int i, j;
    elem_est temp_elem;

    if (esq < dir) {  // verifica se a lista tem n > 1 elementos
        // aleatoriza a seleção do pivot
#ifdef _RANDOMIZA_PIVOT_
        pivot = esq + rand() % (dir+1 - esq);
        SWAP(temp_elem, p_l->item[esq], p_l->item[pivot]);
#endif

        i = esq - 1;
        // procura por um elemento menor que pivot
        for (j = esq; j < dir; j++) {
            // utiliza o últimoi elemento como pivot
            if (MENOR(p_l->item[j], p_l->item[dir])) {
                i++;
                // permuta i-ésimo e o j-ésimo elementos
                SWAP(temp_elem, p_l->item[i], p_l->item[j]);
            }
        }
        i++;
        // permuta o pivot com
        // o último elemento classificado como maior que pivot
        SWAP(temp_elem, p_l->item[dir], p_l->item[i]);

        // recursão para esquerda e direita do pivot
        // conjunto S1 com elementos menores que o pivot
        estQuickSort(p_l, esq, i - 1);
        // conjunto S2 com elementos maiores que o pivot
        estQuickSort(p_l, i + 1, dir);
    }
}

/* Ordena a lista */
void estOrdena(ListaEst *p_l)
{
    estQuickSort(p_l, 0, p_l->tam-1);
    return;
}

/* Remove o elemento que esta no inicio da lista.
   Retorna 0 caso a lista esteja vazia */
int estRemoveInicio(ListaEst *p_l, elem_est *p_e)
{
    int i;

    if (estListaVazia (p_l))
        return 0;

    for (i = 0; i < p_l->tam; i++)
        p_l->item[i] = p_l->item[i + 1];

    p_l->tam--;

    return 1;
}

/* Remove o elemento que esta no final da lista.
   Retorna 0 caso a lista esteja vazia */
int estRemoveFim(ListaEst *p_l, elem_est *p_e)
{
    if (estListaVazia (p_l))
        return 0;

    p_l->tam--;

    return 1;
};

/* Remove o numero de valor e.
   Retorna 0 caso este numero não tenha sido encontrado */
int estRemoveValor(ListaEst *p_l, elem_est e)
{
    int i;

    for (i = 0; i < p_l->tam; i++)
        if (IGUAL(p_l->item[i], e)) break;

    if (i == p_l->tam)
        return 0;

    for (i = i; i < p_l->tam; i++)
        p_l->item[i] = p_l->item[i + 1];

    p_l->tam--;

    return 1;
}

/* Inverte os elementos de uma lista */
void estInverte(ListaEst *p_l)
{
    ListaEst l;
    int i;

    estInicLista (&l);

    for (i = p_l->tam; i > 0; i--)
        estInsereFim (&l, p_l->item[i - 1]);

    estLibera (p_l);

    for (i = 0; i < l.tam; i++)
        estInsereFim (p_l, l.item[i]);
}

/* Remove todos os numeros da lista */
void estLibera(ListaEst *p_l)
{
    estInicLista (p_l);
}

/* Exibe o conteudo da lista */
void estExibe(ListaEst *p_l)
{
    int i;

    if (estListaVazia (p_l)) {
        printf ("Lista vazia!\n");
        return;
    }

    for (i = 0; i < p_l->tam -1; i++)
        printf ("%d ", p_l->item[i].indice);
    // Exibe último item sem espaço no final
    printf ("%d", p_l->item[i].indice);

    printf ("\n");
}

elem_est estObtemElem(ListaEst *p_l, int indice)
{
    return p_l->item[indice];
}
