set term png tiny font "./Roboto-Regular.ttf" 8 size 360,280
set output "images/cpu_est_rand_nomemb.png"
set logscale y 10
set title "Lista estática, com pivot aleatório, sem membro adicional"
set xlabel "tamanho da entrada"
set xtics rotate
set ylabel "tempo de cpu (s)"
set yrange [0.0000001:100000]
set key at 15000,10000
plot \
"resultados/est_rand_nomemb.dat" using 3:4 every ::12::15 title "Sequencial" with linespoints, \
"resultados/est_rand_nomemb.dat" using 3:4 every ::4::7 title "Inversa" with linespoints, \
"resultados/est_rand_nomemb.dat" using 3:4 every ::8::11 title "Aleatória" with linespoints, \
"resultados/est_rand_nomemb.dat" using 3:4 every ::0::3 title "Melhor caso" with linespoints
