set term png tiny font "./Roboto-Regular.ttf" 8 size 360,280
set output "images/wall_est_norand_nomemb.png"
set logscale y 10
set title "Lista estática, sem pivot aleatório, sem membro adicional"
set xlabel "tamanho da entrada"
set xtics rotate
set ylabel "tempo wallclock(s)"
set yrange [0.1:150000]
set key at 15000,100000
plot \
"resultados/est_norand_nomemb.dat" using 3:5 every ::12::15 title "Sequencial" with linespoints, \
"resultados/est_norand_nomemb.dat" using 3:5 every ::4::7 title "Inversa" with linespoints, \
"resultados/est_norand_nomemb.dat" using 3:5 every ::8::11 title "Aleatória" with linespoints, \
"resultados/est_norand_nomemb.dat" using 3:5 every ::0::3 title "Melhor caso" with linespoints
