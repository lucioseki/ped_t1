set term png tiny font "./Roboto-Regular.ttf" 8 size 360,280
set output "images/mem_est_rand_nomemb.png"
set title "Lista estática, com pivot aleatório, sem membro adicional"
set xlabel "tamanho da entrada"
set xtics rotate
set ylabel "consumo de memória (KB)"
set yrange [1000:4000]
set key at 15000,2000
plot \
"resultados/est_rand_nomemb.dat" using 3:6 every ::12::15 title "Sequencial" with linespoints, \
"resultados/est_rand_nomemb.dat" using 3:6 every ::4::7 title "Inversa" with linespoints, \
"resultados/est_rand_nomemb.dat" using 3:6 every ::8::11 title "Aleatória" with linespoints, \
"resultados/est_rand_nomemb.dat" using 3:6 every ::0::3 title "Melhor caso" with linespoints
