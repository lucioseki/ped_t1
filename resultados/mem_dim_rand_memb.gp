set term png tiny font "./Roboto-Regular.ttf" 8 size 360,280
set logscale y 10
set output "images/mem_dim_rand_memb.png"
set title "Lista dinâmica, com pivot aleatório, com membro adicional"
set xlabel "tamanho da entrada"
set xtics rotate
set ylabel "consumo de memória (KB)"
set yrange [1000:1500000]
set key at 15000,10000
plot \
"resultados/dim_rand_memb.dat" using 3:6 every ::12::15 title "Sequencial" with linespoints, \
"resultados/dim_rand_memb.dat" using 3:6 every ::4::7 title "Inversa" with linespoints, \
"resultados/dim_rand_memb.dat" using 3:6 every ::8::11 title "Aleatória" with linespoints, \
"resultados/dim_rand_memb.dat" using 3:6 every ::0::3 title "Melhor caso" with linespoints
