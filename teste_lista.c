/*
 * Programa para testar a ordenação das listas
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lista_dinamica.h"
#include "lista_estatica.h"
#include "timer.h"

int main()
{
#ifdef _IMPRIME_TEMPO_
    // Variáveis para controle da duracao da ordenação
    double inicio_ordena_est = 0;
    double fim_ordena_est = 0;
    double inicio_ordena_dim = 0;
    double fim_ordena_dim = 0;
#endif
#ifdef _IMPRIME_CPU_
    clock_t inicio_cpu_est, fim_cpu_est, inicio_cpu_dim, fim_cpu_dim;
#endif

    int n;
    int indice;

    ListaEst lista_estatica;
    ListaDim lista_dinamica;
    elem_est e_est;
    elem_dim e_dim;

    // Ler as entradas especificadas
    scanf("%d", &n);
    while (n) {
        estInicLista(&lista_estatica);
        dimInicLista(&lista_dinamica);

        while (n--) {
            scanf("%d", &indice);

            estElem(&e_est, indice);
            estInsereFim(&lista_estatica, e_est);

            dimElem(&e_dim, indice);
            dimInsereFim(&lista_dinamica, e_dim);
        }

        GET_TIME(inicio_ordena_est);
#ifdef _IMPRIME_CPU_
        inicio_cpu_est = clock();
#endif
#ifdef _EXEC_EST_
        // Somente a ordenação deve ser feita aqui
        estOrdena(&lista_estatica);
#endif
#ifdef _IMPRIME_CPU_
        fim_cpu_est = clock();
#endif
        GET_TIME(fim_ordena_est);

        GET_TIME(inicio_ordena_dim);
#ifdef _IMPRIME_CPU_
        inicio_cpu_dim = clock();
#endif
#ifdef _EXEC_DIM_
        // Somente a ordenação deve ser feita aqui
        dimOrdena(&lista_dinamica);
#endif
#ifdef _IMPRIME_CPU_
        fim_cpu_dim = clock();
#endif
        GET_TIME(fim_ordena_dim);

        // Exibir saída do programa
        // Dica: use os comandos abaixo para exibir o tempo da ordenação
#ifdef _IMPRIME_TAMANHO_
        printf("est %d ", lista_estatica.tam);
#endif
#ifdef _IMPRIME_CPU_
        printf("cpu %f wall ", ((double)(fim_cpu_est - inicio_cpu_est)) / CLOCKS_PER_SEC);
#endif
#ifdef _IMPRIME_TEMPO_
        printf("%.0f\n", fim_ordena_est - inicio_ordena_est);
#endif
#ifdef _IMPRIME_LISTA_
#ifdef _EXEC_EST_
        estExibe(&lista_estatica);
#endif
#endif
#ifdef _IMPRIME_TAMANHO_
        printf("dim %d ", dimTamanho(&lista_dinamica));
#endif
#ifdef _IMPRIME_CPU_
        printf("cpu %f wall ", ((double)(fim_cpu_dim - inicio_cpu_dim)) / CLOCKS_PER_SEC);
#endif
#ifdef _IMPRIME_TEMPO_
        printf("%.0f\n", fim_ordena_dim - inicio_ordena_dim);
#endif
#ifdef _IMPRIME_LISTA_
#ifdef _EXEC_DIM_
        dimExibe(&lista_dinamica);
#endif
#endif

        // Liberar memória alocada
        estLibera(&lista_estatica);
        dimLibera(&lista_dinamica);

        scanf("%d", &n);
    }

    return 0;
}
